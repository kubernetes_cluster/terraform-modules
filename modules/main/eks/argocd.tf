provider "gitlab" {
  token = var.gitlab_token
}

locals {
  argocd = {
    gitlab_private_repo_name = "kubernetes_cluster/helm_chart"
    version                  = "5.52.1"
    namespace                = "argocd"
  }

  root = {
    namespace      = "argocd",
    path           = "applications/",
    repoURL        = "git@gitlab.com:${local.argocd.gitlab_private_repo_name}.git",
    targetRevision = "main",
  }
}


# ED25519 key for ArgoCD
resource "tls_private_key" "ed25519_argocd" {
  algorithm = "ED25519"
}

resource "gitlab_deploy_key" "k8s_repo" {
  project = local.argocd.gitlab_private_repo_name
  title   = "ArgoCD Deploy key for deployment related to ${var.deployment_prefix}"
  key     = tls_private_key.ed25519_argocd.public_key_openssh
}

resource "helm_release" "argocd" {
  name = "argocd"

  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  version          = local.argocd.version
  namespace        = local.argocd.namespace
  create_namespace = true

  values = [
    file("${path.module}/templates/values.yaml", {
      k8s_ssh_private_key = tls_private_key.ed25519_argocd.private_key_openssh,
      k8s_repo            = local.argocd.gitlab_private_repo_name,
    })
  ]

  depends_on = [
    module.eks
  ]
}


##################### Deploy initial bootstrap ArgoCD app #####################

provider "kubectl" {
  apply_retry_count      = 5
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  load_config_file       = false

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "aws"
    # This requires the awscli to be installed locally where Terraform is executed
    args = ["eks", "get-token", "--cluster-name", module.eks.cluster_id]
  }
}

resource "kubectl_manifest" "argocd_root" {
  yaml_body = templatefile("${path.module}/templates/root.yaml", {
    namespace      = local.argocd.namespace
    path           = local.root.path
    repoURL        = local.root.repoURL
    targetRevision = local.root.targetRevision
  })
}
