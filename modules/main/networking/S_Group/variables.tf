variable "alow_ports" {
  type    = list(any)
  default = ["80", "443"]
}

variable "vpc_id" {
  type    = string
  default = ""
}

variable "ssh_cidr_block" {
  type    = list(any)
  default = []

}
