variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}


variable "public_subnets" {
  type = list(any)
  default = [
    "10.0.10.0/24",
    "10.0.16.0/24"
  ]

}

variable "private_subnets" {
  type = list(any)
  default = [
    "10.0.11.0/24",
    "10.0.17.0/24"
  ]
}



variable "database_subnets" {
  type = list(any)
  default = [
  ]
}



variable "intra_subnets" {
  type = list(any)
  default = [
  ]
}


variable "deployment_prefix" {
  description = "Prefix of the deployment"
  type        = string
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks."
  type        = bool
  default     = false
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone."
  type        = bool
  default     = false
}
