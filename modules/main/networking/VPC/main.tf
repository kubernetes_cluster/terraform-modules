data "aws_availability_zones" "available" {}

locals {
  eks_cluster_name = "${var.deployment_prefix}-eks-cluster"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.4.0"

  name             = "${var.deployment_prefix}-vpc"
  cidr             = var.vpc_cidr
  azs              = data.aws_availability_zones.available.names
  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets
  #intra_subnets                      = var.intra_subnets
  enable_nat_gateway                 = true
  single_nat_gateway                 = var.single_nat_gateway
  one_nat_gateway_per_az             = var.one_nat_gateway_per_az
  enable_dns_hostnames               = true
  create_igw                         = true
  create_database_subnet_route_table = true

  tags = {
    "Name" = "${var.deployment_prefix}-VPC"
  }

  public_subnet_tags = {
    "Name"                                            = "public-subnet-${var.deployment_prefix}"
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"                          = "1"
  }

  private_subnet_tags = {
    "Name"                                            = "private-subnet-${var.deployment_prefix}"
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"                 = "1"
  }

  database_subnet_tags = {
    "Name" = "database-subnet-${var.deployment_prefix}"
  }

  intra_subnet_tags = {
    "Name" = "intra-subnet-${var.deployment_prefix}"
  }

  intra_route_table_tags = {
    "Name" = "intra-route-table-${var.deployment_prefix}"
  }

  public_route_table_tags = {
    "Name" = "public-route-table-${var.deployment_prefix}"
  }

  database_route_table_tags = {
    "Name" = "database-route-table-${var.deployment_prefix}"
  }

  private_route_table_tags = {
    "Name" = "private-route-table-${var.deployment_prefix}"
  }
}
